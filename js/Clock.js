'use strict';

function Clock () {
	this.getTime = function (stage) {
		var result = '',
			date = new Date();

		switch(stage) {
			case 'short': 
				result = getHours() + getMinutes();
				break;
			case 'long':
				result = getHours() + getMinutes() + getSeconds();
				break;
			case 'date':
				result = showDate();
				break;	
		}

		function getHours() {
			return date.getHours() > 9 ? date.getHours() : '0' + date.getHours();
		}

		function getMinutes() {
			return date.getMinutes() > 9 ? ':' + date.getMinutes() : ':0' + date.getMinutes();
		}

		function getSeconds() {
			return date.getSeconds() > 9 ? ':' + date.getSeconds() : ':0' + date.getSeconds();
		}

		function getMonth() {
			return (date.getMonth() + 1) > 9 ? (date.getMonth() + 1) : '0' + (date.getMonth() + 1);
		}

		function showDate() {
			return date.getDate() + '/' + getMonth() + '/' + date.getFullYear();
		}

		return result;	
	}

	return this;
}