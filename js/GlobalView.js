'use strict';

function GlobalView (_attributes) {
	var attr = {
			container: '',
			conversation: '',
			mediator: '',
			dialogues: [],
			id: 0,
			deleteInformation: false,
			defaultMessages: false
		};

	initialize();

	function initialize () { // Ask about it
		unboxing();

		if (attr.defaultMessages) {
			attr.conversation.add('Hello!');
			attr.conversation.add('My name is S. What\'s yours?');
		}

		render();
	}

	function unboxing () {
		for (var key in _attributes) {
			attr[key] = _attributes[key];
		}

		if (attr.dialogues > attr.maxDialogues) {
			attr.dialogues = attr.maxDialogues;
		}
	}

	function render () {
		container.addEventListener('click', action, false);
	}

	function action (e) {
		var target = e.target,
			parent = target.parentNode,
			value;

		if (target.hasAttribute('data-action')) {
			if (target.getAttribute('data-action') == 'add') {
				if (attr.dialogues.length < attr.maxDialogues) {
					var box = new DialogueBox(attr.conversation, attr.dialogues.length, attr.id++, attr.mediator);

					attr.dialogues.push(box);

					attr.container.appendChild(box.getBox());
				}
			} 
		}

		if (target.getAttribute('data-action') == 'delete') {
			value = parent.parentNode.getAttribute('data-id');
			target = attr.container.querySelector('section[data-id="' + value + '"');
			
			attr.dialogues[value].deleteEvents();
			attr.container.removeChild(target);

			attr.dialogues.splice(value, 1);
		}
	}

	return this;
}