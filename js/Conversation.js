'use strict';

function Conversation () {
	var dialogue = [];

	this.add = function (answer) {
		dialogue.push(answer);
	};

	this.forEach = function (fn) {
		dialogue.forEach(fn);
	};

	return this;
}