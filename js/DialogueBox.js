function DialogueBox (conversation, length, id, mediator) {
	var attr = {
			container: '',
			body: '',
			textarea: '', 
			buttons: {
				send: '',
				refresh: '',
				delete: ''
			}
		};

	init();

	this.getBox = function () {
		return attr.container;
	};

	this.deleteEvents = function () {
		attr.buttons.send.removeEventListener('click', sendMessage, false);
		attr.buttons.refresh.removeEventListener('click', updateMessages, false);
	};

	function init () {
		var box = document.createElement('section');
		box.classList.add('dialogue');
		box.setAttribute('data-id', length);
		box.innerHTML = createBox();

		attr.container = document.createDocumentFragment();
		attr.container.appendChild(box);

		attr.buttons.send = attr.container.querySelector('.btn'),
		attr.buttons.refresh = attr.container.querySelector('div[data-action="refresh"]');
		attr.buttons.delete = attr.container.querySelector('div[data-action="delete"');
		attr.textarea = attr.container.querySelector('textarea');
		attr.body = attr.container.querySelector('.dialogue-body');

		attr.buttons.send.addEventListener('click', sendMessage, false);
		attr.buttons.refresh.addEventListener('click', updateMessages, false);
		mediator.addEventListener(updateMessages);
	}

	function sendMessage (e) {
		var value = attr.textarea.value;

		if (value) {
			conversation.add(value);

			updateMessages ();

			mediator.trigger();
		} else {
			alert('You can\'t send empty message');
		}
	}

	function updateMessages () {
		var messages = document.createElement('div');
		messages.innerHTML = createMessages();
		attr.body.innerHTML = "";
				
		attr.body.appendChild(messages);	
	}


	function createBox () {
		var tpl = '<article class="dialogue-header">\
							<div>Dialog :_id</div>\
							<div data-action="refresh">Refresh</div>\
							<div data-action="delete">Delete</div>\
						</article>\
						<article class="dialogue-body">\
							:messages\
						</article>\
						<article class="dialogue-foot">\
						<textarea></textarea><button class="btn">Send</button>\
						</article>';

		return template(tpl, {messages: createMessages(), _id: id+1});
	}

	function createMessages () {
		var tpl = '<div class="message"><span class="text">:text</span><span class="data">:date</span></div>',
			result = '',
			keys = {};

		conversation.forEach(function (message) {
			keys = {
					text: message,
					date: new Clock().getTime('long')
				};

			result += template(tpl, keys);
		});

		return result;
	}

	function template (tpl, data) {
		for (var key in data) {
			tpl = tpl.replace(':' + key, data[key]);
		}

		return tpl;
	}

	return this;
}