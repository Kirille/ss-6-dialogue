'use strict';

document.addEventListener('DOMContentLoaded', init, false);

function init () {
	var attributes = {
			container: document.querySelector('#container').querySelector('.dialogues'),
			conversation: new Conversation(),
			mediator: new Mediator(),
			dialogues: [],
			maxDialogues: 9,
			deleteInformation: false,
			defaultMessages: true
		};

	new GlobalView(attributes);
}